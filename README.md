<h1 align="center">HITS Langman</h1>

<p align="center">
Langman is a language files manager in your artisan console, it helps you search, update, add, and remove
translation lines with ease. Taking care of a multilingual interface is not a headache anymore.
<br>
<br>


<br>
<a href="https://travis-ci.com/gitlab/mah.shamim/hits-langman"><img src="https://travis-ci.com/mah.shamim/hits-langman.svg?branch=master" alt="Build Status"></a>
<a href="https://gitlab.styleci.io/repos/17121267"><img src="https://gitlab.styleci.io/repos/17121267/shield?style=flat" alt="StyleCI"></a>
<a href="https://packagist.org/packages/mahshamim/hits-langman"><img src="https://img.shields.io/packagist/v/mahshamim/hits-langman.svg?style=flat-round" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/mahshamim/hits-langman"><img src="https://img.shields.io/packagist/dt/mahshamim/hits-langman.svg?style=flat-round" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/mahshamim/hits-langman"><img src="https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-round" alt="License"></a>
</p>

## Installation

Begin by installing the package through Composer. Run the following command in your terminal:

```
$ composer require mahshamim/laravel-langman
```

Once done, add the following line in your providers array of `config/app.php`:

```php
Mahshamim\Langman\LangmanServiceProvider::class;
```

This package has a single configuration option that points to the `resources/lang` directory, if only you need to change
the path then publish the config file:

```
php artisan vendor:publish --provider="Mahshamim\Langman\LangmanServiceProvider"
```

## Usage

### Showing lines of a translation file

```
php artisan langman:show users
```

You get:

```
+---------+---------------+-------------+
| key     | en            | nl          |
+---------+---------------+-------------+
| name    | name          | naam        |
| job     | job           | baan        |
+---------+---------------+-------------+
```

---

```
php artisan langman:show users.name
```

Brings only the translation of the `name` key in all languages.

---

```
php artisan langman:show users.name.first
```

Brings the translation of a nested key.

---

```
php artisan langman:show package::users.name
```

Brings the translation of a vendor package language file.

---

```
php artisan langman:show users --lang=en,it
```

Brings the translation of only the "en" and "it" languages.

---

```
php artisan langman:show users.nam -c
```

Brings only the translation lines with keys matching the given key via close match, so searching for `nam` brings values for
keys like (`name`, `username`, `branch_name_required`, etc...).

In the table returned by this command, if a translation is missing it'll be marked in red.

### Finding a translation line

```
php artisan langman:find 'log in first'
```

You get a table of language lines where any of the values matches the given phrase by close match.

### Searching view files for missing translations

```
php artisan langman:sync
```

This command will look into all files in `resources/views` and `app` and find all translation keys that are not covered in your translation files, after
that it appends those keys to the files with a value equal to an empty string.

### Filling missing translations

```
php artisan langman:missing
```

It'll collect all the keys that are missing in any of the languages or has values equals to an empty string, prompt
asking you to give a translation for each, and finally save the given values to the files.

### Translating a key

```
php artisan langman:trans users.name
php artisan langman:trans users.name.first
php artisan langman:trans users.name --lang=en
php artisan langman:trans package::users.name
```

Using this command you may set a language key (plain or nested) for a given group, you may also specify which language you wish to set leaving the other languages as is.

This command will add a new key if not existing, and updates the key if it is already there.

### Removing a key

```
php artisan langman:remove users.name
php artisan langman:remove package::users.name
```

It'll remove that key from all language files.

### Renaming a key

```
php artisan langman:rename users.name full_name
```

This will rename `users.name` to be `users.full_name`, the console will output a list of files where the key used to exist.

## Notes

`langman:sync`, `langman:missing`, `langman:trans`, and `langman:remove` will update your language files by writing them completely, meaning that any comments or special styling will be removed, so I recommend you backup your files.

## Web interface

If you want a web interface to manage your language files instead, I recommend [Laravel 5 Translation Manager](https://github.com/barryvdh/laravel-translation-manager)
by [Barry vd. Heuvel](https://github.com/barryvdh).
