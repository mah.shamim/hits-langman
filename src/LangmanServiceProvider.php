<?php

namespace Mahshamim\Langman;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class LangmanServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/langman.php' => config_path('langman.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/langman.php', 'langman');

        $this->app->bind(Manager::class, function () {
            return new Manager(
                new Filesystem(),
                $this->app['config']['langman.path'],
                array_merge($this->app['config']['view.paths'], [$this->app['path']])
            );
        });

        $this->commands([
            \Mahshamim\Langman\Commands\MissingCommand::class,
            \Mahshamim\Langman\Commands\RemoveCommand::class,
            \Mahshamim\Langman\Commands\TransCommand::class,
            \Mahshamim\Langman\Commands\ShowCommand::class,
            \Mahshamim\Langman\Commands\FindCommand::class,
            \Mahshamim\Langman\Commands\SyncCommand::class,
            \Mahshamim\Langman\Commands\RenameCommand::class,
        ]);
    }
}
