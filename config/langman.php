<?php

$path = base_path('resources/lang');

if (!is_dir($path)) {
    $path = base_path('lang');
}

return [
    /*
     * --------------------------------------------------------------------------
     * Path to the language directories
     * --------------------------------------------------------------------------
     *
     * This option determines the path to the languages directory, it's where
     * the package will be looking for translation files. These files are
     * usually located in resources/lang but you may change that.
     */

    'path' => realpath($path),
];
